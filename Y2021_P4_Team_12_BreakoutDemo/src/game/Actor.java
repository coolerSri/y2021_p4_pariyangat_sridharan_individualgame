package game;

import javafx.scene.image.ImageView;

public abstract class Actor extends ImageView {

	public abstract void act(long now);

	public Actor() {
		
	}

	public void move(double dx, double dy) {
		setX(getX() + dx);
		setY(getY() + dy);
	}

	public World getWorld() {
		
	}

	public double getHeight() {
		return super.getFitHeight();
	}

	public <A extends Actor> java.util.List<A> getIntersectingObjects(Class<A> cls) {

	}

	public <A extends Actor> java.util.List<A> getOneIntersectingObject(Class<A> cls) {
		
	}

}
