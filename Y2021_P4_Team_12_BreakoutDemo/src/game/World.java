package game;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public abstract class World<A> extends Pane {

	private AnimationTimer timer;

	// Abstract Methods
	public abstract void act(long now);

	public World() {
		timer = new AnimationTimer() {

			@Override
			public void handle(long now) {
				act(now);
				List<Actor> actors = getObjects(getThisThing());
				for (Actor a : actors) {
					a.act(now);
				}
			}
		};
	}
	
	private Class<A> getThisThing() {
		return (Class<A>) getClass();
	}

	public void add(Actor actor) {
		getChildren().add(actor);
	}

	public void remove(Actor actor) {
		getChildren().remove(actor);
	}

	public void start() {
		timer.start();
	}

	public void stop() {
		timer.stop();
	}

	// If you are confused, just draw the structure of the class, what you think it is
	// on paper, and everything will make sense! It worked for me!
	public <A extends Actor> List<A> getObjects(Class<A> cl) {
		List<A> realActors = new ArrayList<>();
		ObservableList<Node> allActors = getChildren();
		
		for (Node actor : allActors) {
			if (actor.getClass().isInstance(cl)) {
				realActors.add((A) actor);
			}
		}
		return realActors;
	}

}
